package com.suchaj.hms

import androidx.multidex.MultiDexApplication

class MainApplication: MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()
        ContexterManager.getInstance().setApplicationContext(this)

    }
}